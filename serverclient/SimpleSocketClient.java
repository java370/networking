import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class SimpleSocketClient {
    public void go() {
        try {
            Socket s = new Socket("127.0.0.1", 4242);
            OutputStream stream = s.getOutputStream();
            PrintWriter writer = new PrintWriter(stream);
            writer.println("Hello World!");
            System.out.println("Request Sent");
            writer.flush();
            writer.close();
            // s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SimpleSocketClient client = new SimpleSocketClient();
        client.go();
    }
}