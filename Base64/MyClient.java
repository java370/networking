import java.net.*;
import java.util.*;
import java.io.*;

public class MyClient {

    private static void RetriveFile(byte[] DecodedArray) throws Exception {
        String sinkFile = "Decoded_File";
        FileOutputStream fos = new FileOutputStream(sinkFile);
        DataOutputStream dos = new DataOutputStream(fos);
        BufferedOutputStream bos = new BufferedOutputStream(dos);
        bos.write(DecodedArray);
        bos.flush();
        bos.close();

    }

    private static byte[] DecodeInBase64(String EncodedTextInBase64) throws Exception {
        return Base64.getDecoder().decode(EncodedTextInBase64);
    }

    private static void go() {
        try {

            Scanner sc = new Scanner(System.in);
            Socket s = new Socket("127.0.0.1", 4242);

            // reader
            InputStream is = s.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String EncodedTextInBase64 = br.readLine();
            byte[] decodedArray = DecodeInBase64(EncodedTextInBase64);
            RetriveFile(decodedArray);

            s.close();
            sc.close();
            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MyClient client = new MyClient();
        client.go();
    }

}
