import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class BiDirectionalSocketServer {
    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(4242);
            while (true) {
                Socket socket = serverSocket.accept();

                // Reading

                InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                String requString = reader.readLine();

                // Writing
                OutputStream streamWriter = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(streamWriter);
                writer.println(requString.toUpperCase());
                writer.flush();

                // if (requString.equals("sajal")) {
                //     System.out.println("terminating Command!");
                //     serverSocket.close();
                // }

                System.out.println("Received : " + requString);
                System.out.println("Sent : " + requString.toUpperCase());

                reader.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }

    public static void main(String[] args) {
        BiDirectionalSocketServer server = new BiDirectionalSocketServer();
        server.go();
    }
}
