
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class simpleClient {
    public void go() {
        try {
            Socket s = new Socket("127.0.0.1", 4242);
            OutputStream stream = s.getOutputStream();
            PrintWriter writer = new PrintWriter(stream);
            InputStreamReader streamReader = new InputStreamReader(s.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);
            String myString = reader.readLine();
            writer.println(myString);
            System.out.println("Request Sent");

            String kk = reader.readLine();
            writer.println(kk);
            System.out.println("Request Sent");

            writer.flush();
            writer.close();
            // s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        simpleClient client = new simpleClient();
        client.go();
    }
}
