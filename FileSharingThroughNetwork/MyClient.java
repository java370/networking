import java.io.*;
import java.net.*;
import java.util.*;

public class MyClient {
    public void go() {
        try {

            Socket s;
            PrintWriter writer;

            int count = 5;
            Scanner sc = new Scanner(System.in);
            s = new Socket("127.0.0.1", 4242);
            // Writer
            OutputStream stream = s.getOutputStream();
            writer = new PrintWriter(stream);

            // OUTPUT FILE NAME;
            String sinkFile = "m_";

            FileOutputStream fos = new FileOutputStream(sinkFile);
            DataOutputStream dos = new DataOutputStream(fos);
            BufferedOutputStream bos = new BufferedOutputStream(dos);

            // while (count-- > 0) {

            System.out.print("Enter File Name : ");
            String myString = sc.nextLine();
            writer.println(myString);
            writer.flush();

            DataInputStream dIn = new DataInputStream(s.getInputStream());
            BufferedInputStream Bis = new BufferedInputStream(dIn);

            int length = dIn.readInt(); // read length of incoming message

            // process - 1
            int availableBits = 0;
            byte[] byteToRead = null;
            while ((availableBits = Bis.available()) != 0) {
                byteToRead = new byte[2000];
                // System.out.println(Bis.available());
                Bis.read(byteToRead);
                bos.write(byteToRead);
            }
            bos.flush();

            // process -2
            // if (length > 0) {
            // byte[] message = new byte[length];
            // dIn.readFully(message);
            // // dIn.readFully(message, 0, message.length);
            // bos.write(message);
            // bos.flush();
            // } else {
            // System.out.println("File Not Found!");
            // }

            // process - 3

            // if (length > 0) {
            // while (length-- > 0) {
            // bos.write(dIn.readByte());
            // }
            // bos.flush();
            // } else {
            // System.out.println("File Not Found!");
            // }

            // }
            sc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MyClient client = new MyClient();
        client.go();
    }
}