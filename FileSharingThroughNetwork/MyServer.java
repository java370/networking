import java.io.*;
import java.net.*;
import java.util.*;

public class MyServer {
    public void go() throws Exception {
        try {
            ServerSocket serverSocket = new ServerSocket(4242);
            while (true) {
                Socket socket = serverSocket.accept();

                // Reading

                InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);

                // Send Byte
                DataOutputStream dOut = new DataOutputStream(socket.getOutputStream());

                // Get the filename here
                String requString = reader.readLine();
                File checkFile = new File(requString);
                // reader.close();

                
                // check file validation
                if (checkFile.isFile() && !checkFile.isDirectory()) {
                    byte[] getFileByte = MakeFile(checkFile);
                    // System.out.println(getFileByte.length);

                    dOut.writeInt(getFileByte.length); // write length of the message
                    dOut.write(getFileByte); // write the message
                    System.out.println("File Found Sending...");

                } else {
                    // System.out.println("File Not Found From Ser");
                    dOut.writeInt(0); // write length of the message
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }

    public static byte[] MakeFile(File sourceFile) throws Exception {
        FileInputStream fis = new FileInputStream(sourceFile);
        DataInputStream dis = new DataInputStream(fis);
        BufferedInputStream bis = new BufferedInputStream(dis);

        int availableBits = 0;
        byte[] byteToRead = null;
        while ((availableBits = bis.available()) != 0) {
            byteToRead = new byte[availableBits];
            bis.read(byteToRead);
        }
        dis.close();
        fis.close();
        return byteToRead;
    }

    public static void main(String[] args) {
        MyServer server = new MyServer();
        try {
            server.go();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
