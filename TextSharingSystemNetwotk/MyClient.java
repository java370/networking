import java.io.*;
import java.net.*;
import java.util.*;

public class MyClient {
    public void go() {
        try {

            Scanner sc = new Scanner(System.in);
            Socket s = new Socket("127.0.0.1", 4242);

            // reader
            InputStream is = s.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            String line = null;

            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            s.close();
            sc.close();
            // writer.close();
            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MyClient client = new MyClient();
        client.go();
    }
}