import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class BiDirectionalSocketClient_2 {
    public void go() {
        Socket s;
        PrintWriter writer;

        try {
            int count = 3;
            Scanner sc = new Scanner(System.in);
            s = new Socket("127.0.0.1", 4242);
            // Writer
            OutputStream stream = s.getOutputStream();
            writer = new PrintWriter(stream);
            // Read
            InputStreamReader streamReader = new InputStreamReader(s.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);
            while (count-- > 0) {

                String myString = sc.nextLine();
                writer.println(myString);
                System.out.println("Request Sent");
                writer.flush();
                String response = reader.readLine();
                System.out.println("Response Received : " + response);

            }
            sc.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    public static void main(String[] args) {
        BiDirectionalSocketClient_2 client = new BiDirectionalSocketClient_2();
        client.go();
    }
}
