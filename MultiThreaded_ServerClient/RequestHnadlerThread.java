import java.io.*;
import java.net.*;

public class RequestHnadlerThread extends Thread {

    private Socket socket;

    public RequestHnadlerThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            // Reading
            InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
            BufferedReader reader = new BufferedReader(streamReader);

            // Writing
            OutputStream streamWriter = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(streamWriter);

            String requString = null;

            while (true) {
                requString = reader.readLine();
                writer.println(requString.toUpperCase());
                writer.flush();

                System.out.println("Received : " + requString);
                System.out.println("Sent : " + requString.toUpperCase());
            }
            // reader.close();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

}
