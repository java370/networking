import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class BiDirectionalSocketServer {
    public void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(4242);
            Socket socket = null;
            while (true) {

                socket = serverSocket.accept();

                // Reading

                RequestHnadlerThread handler = new RequestHnadlerThread(socket);
                handler.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }

    public static void main(String[] args) {
        BiDirectionalSocketServer server = new BiDirectionalSocketServer();
        server.go();
    }
}
