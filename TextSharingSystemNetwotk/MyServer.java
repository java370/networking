import java.io.*;
import java.net.*;
import java.util.*;

public class MyServer {
    public void go() throws Exception {
        try {
            ServerSocket serverSocket = new ServerSocket(4242);
            while (true) {
                Socket socket = serverSocket.accept();

                File checkFile = new File("m.txt");

                // Set-up Output Connection
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);

                // check file validation
                if (checkFile.exists() && !checkFile.isDirectory()) {

                    FileReader fr = new FileReader(checkFile);
                    BufferedReader br = new BufferedReader(fr);

                    System.out.println("File Found Sending...");
                    int c;
                    while ((c = br.read()) != -1) {
                        bw.write((char) c);
                        bw.flush();
                    }
                    br.close();
                    System.out.println("File Sent from Server!");
                } else {
                    // System.out.println("File Not Found From Ser");
                }
                socket.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }

    public static void main(String[] args) {
        MyServer server = new MyServer();
        try {
            server.go();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
