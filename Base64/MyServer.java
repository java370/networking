import java.net.*;
import java.util.*;
import java.io.*;

public class MyServer {

    private static void ShowInBrowser(Socket socket, String EncodedTextInBase64) throws Exception {
        OutputStream os = socket.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write("HTTP/1.1 200 OK\r\n");
        bw.write("Date: Tue, 09 May 2022 01:11:34 GMT\r\n");
        bw.write("Server: Apache/1.3.3.7 (Unix) (MacOS)\r\n");
        bw.write("Last-Modified: Tue, 09 Mar 2022 01:11:34 GMT\r\n");
        bw.write("Content-Length: " + EncodedTextInBase64.length() + "\r\n");
        bw.write("Content-Type: text/html; charset=UTF-8\r\n");
        bw.write("\r\n");
        bw.write(EncodedTextInBase64);
        bw.flush();
        System.out.println("Sent To Browser!");
    }

    private static void SendToClient(Socket socket, String EncodedTextInBase64) throws Exception {
        OutputStream os = socket.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write(EncodedTextInBase64);
        bw.flush();
        System.out.println("Sent To Client!");
    }

    private static String EncodeInBase64(byte[] byteFileData) throws IOException {
        return Base64.getEncoder().encodeToString(byteFileData);
    }

    private static byte[] MakeFile(File sourceFile) throws Exception {
        FileInputStream fis = new FileInputStream(sourceFile);
        DataInputStream dis = new DataInputStream(fis);
        BufferedInputStream bis = new BufferedInputStream(dis);

        int availableBits = 0;
        byte[] byteToRead = null;
        while ((availableBits = bis.available()) != 0) {
            byteToRead = new byte[availableBits];
            bis.read(byteToRead);
        }
        dis.close();
        fis.close();
        return byteToRead;
    }

    private static void go(String fileName) throws Exception {
        try {

            ServerSocket serverSocket = new ServerSocket(4242);

            while (true) {
                Socket socket = serverSocket.accept();
                File checkFile = new File(fileName);

                if (checkFile.exists() && !checkFile.isDirectory()) {
                    byte[] byteFileData = MakeFile(checkFile);
                    String EncodedTextInBase64 = EncodeInBase64(byteFileData);

                   
                    SendToClient(socket, EncodedTextInBase64);

                    // ShowInBrowser(socket, EncodedTextInBase64);

                } else {
                    System.out.println("File Not Found From Server Directory");
                }
                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            MyServer.go(args[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}